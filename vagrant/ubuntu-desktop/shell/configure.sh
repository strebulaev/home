set -x && \
mkdir -p ~/.config/Code/User/ && \
echo '{"sync.gist": "88dedc00e291edcc6d7c60a6b0241e3e"}' > ~/.config/Code/User/settings.json && \
code --install-extension Shan.code-settings-sync && \
DISPLAY=:0 dbus-launch --exit-with-session gsettings set com.canonical.Unity.Launcher favorites \
    "$(gsettings get com.canonical.Unity.Launcher favorites | sed -e "s/]/, 'application:\/\/google-chrome.desktop', 'application:\/\/keepass2.desktop', 'application:\/\/code.desktop']/")"
set -x && \
apt-add-repository ppa:dlech/keepass2-plugins && \
apt-add-repository ppa:jtaylor/keepass && \
wget -q -O - https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add - && \
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add - && \
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list' && \
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list' && \
sh -c "echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/google.list" && \
apt-get update && \
apt-get install -y git keepass2 keepass2-plugin-keeagent code google-chrome-stable p7zip-full dotnet-sdk-2.0.0